<?php
    function checkCircuits($int): array{
        $resultArray = [];
        $errors = array(2=>"Left arm", 3=>"Right arm", 5=>"Motherboard", 7=>"Processor", 11=>"Zip Defluxer", 13=>"Motor");
        if($int%2==0) {
            array_push($resultArray, $errors[2]);
        }
        if($int%3==0){
            array_push($resultArray, $errors[3]);
        }
        if($int%5==0){
            array_push($resultArray, $errors[5]);
        }
        if($int%7==0){
             array_push($resultArray, $errors[7]);
         }
        if($int%11==0){
             array_push($resultArray, $errors[11]);
         }
        if($int%13==0){
             array_push($resultArray, $errors[13]);
         }
        return $resultArray;
    }
?>
