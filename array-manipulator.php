<?php
function reverse($arr): Array {return array_reverse($arr);}
function push($arr, ...$str): Array {return [...$str];}
function sum($arr): int|float {return array_sum($arr);}
function arrayContains($arr, $obj): bool {return in_array($obj, $arr);}
function merge($arr, ...$args): Array {    
    return  array_merge($arr, ...$args);
}
?>
