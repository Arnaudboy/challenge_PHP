<?php
    date_default_timezone_set("Europe/Paris");
    $hour = date("h:i:sa");

    if ($hour > date("h:i:sa", strtotime("6:00am")) && $hour < date("h:i:sa", strtotime("12:00pm"))) {
        echo "Hello! Have a nice day :)\n";
    } elseif ($hour >= date("h:i:sa", strtotime("12:00pm")) && $hour <= date("h:i:sa", strtotime("6:00pm"))) {
        echo "Have a good afternoon!\n";
    } elseif ($hour > date("h:i:sa", strtotime("6:00pm")) && $hour <= date("h:i:sa", strtotime("9:00pm"))) {
        echo "Good evening! Hope you had a good day!\n";
    } else {
        echo "Good night! See you tomorrow :)\n";
    }
?>
