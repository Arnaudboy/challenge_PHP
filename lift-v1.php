<?php
    function getFloor(int $actualFloor, ?int $requestedFloor, ?array $desiredFloor): ?int
    {
        if (empty($desiredFloor)) {
            if ($requestedFloor == null) {
                return null;
            }
            return $requestedFloor;
        } else {
            if($requestedFloor){
               $tmp = abs($actualFloor - $requestedFloor); 
            } else {
                $tmp = 10000000;
            }
            
            $res = $requestedFloor;
            foreach ($desiredFloor as $floor) {
                if (abs($floor - $actualFloor) < $tmp) {
                    $tmp = abs($floor - $actualFloor);
                    $res = $floor;
                }
            }
            return $res;
        }
    }

    function getDirection(int $actualFloor, ?int $requestedFloor, ?array $desiredFloor): ?int
    {
        $floor = getFloor($actualFloor, $requestedFloor, $desiredFloor);
        if ($floor > $actualFloor) {
            return 1;
        } elseif ($floor < $actualFloor) {
            return -1;
        }
        return 0;
    } 
?>
