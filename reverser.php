<?php
    function reverse(string $str): string {
        return strrev($str);
    }
    function isPalindrome(string $str): bool {
            if ($str == strrev($str)) {
                return True;
            }
            return False;
    }
?>
