<?php
    Class Car {
    private $tank;

    public function setTank($qt) {
        $this->tank = $qt;
        return $this;
    }

    public function ride($distance){
        $this->tank -= ($distance/20);
        return $this;
    }

    public function getTank() {
        return $this->tank;
    }
}
?>
