<?php
    function myArrayReduce($arr, callable $func, $startPoint=null) {
        if(sizeof($arr)==0){
            return $startPoint;
        }
        $reduce = $func($startPoint, $arr[0]);
        for($i=1;$i<sizeof($arr);$i++) {
            $reduce = $func($reduce, $arr[$i]);
        }
        return $reduce;
    }
?>
