<?php
    function manageMovements($str): array {
        $splitedString = str_split($str);
        $mooves = array("F"=>"FRONT", "B"=>"BACKWARDS", "R"=>"RIGHT", "L"=>"LEFT");
        $returnedArray = [];
        for($i=0;$i<sizeof($splitedString);$i++){
            if($i>0){
                    if($splitedString[$i]==$splitedString[$i-1]){
                        array_push($returnedArray, $mooves[$splitedString[$i]]." AGAIN");
                        continue;
                    }
            }
            array_push($returnedArray, $mooves[$splitedString[$i]]);
        }
        return $returnedArray;
    }
?>
