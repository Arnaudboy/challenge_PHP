<?php
function explodeWords(string $string, string $separator="\s", int $limit = PHP_INT_MAX): array {
    $absLimit = abs($limit);
    if($separator=="/"){
        $separator = "/\//";
    }else{
        $separator = "/".$separator."/";
    }
    if($limit>0){    
        $splitedString = preg_split($separator, $string, $limit);
    }
    if ($limit<0){
        $splitedString = preg_split($separator, $string);
        return array_slice($splitedString,0, $limit);
    }
    return $splitedString;
}
?>
