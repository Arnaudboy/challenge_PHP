<?php
    function dnaDiff($str1, $str2): int|bool {
        $count = 0;
        if(strlen($str1)!=strlen($str2)){
            return False;
        }
        for($i=0;$i<strlen($str1);$i++){
                if($str1[$i]!=$str2[$i]){
                    $count++;
                }
        }
        return $count;
    }
?>
