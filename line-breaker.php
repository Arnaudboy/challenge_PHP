<?php
    function breakLines(string $str, int $i): string {
            $arr = preg_split("/(.{1,$i}\b|.{$i})\K(?: +|\B|\Z)/", $str);
            return trim(join("\n", $arr));
    }
echo breakLines("Line with words", 15);
?>
