<?php
function myArrayMap(?callable $callback, array $array) : array{
    if(!$callback) { return $array; }

    $res = [];
    foreach($array as $elem) {
        array_push($res, $callback($elem));
    }
    return $res;
}
?>
