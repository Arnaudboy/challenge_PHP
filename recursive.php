<?php
    function factorial(int $nbr): int|float {
        if ($nbr <= 1){
            return 1;
        }
        return $nbr * factorial($nbr-1);
    }
?>
