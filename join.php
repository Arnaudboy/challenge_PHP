<?php
function joinWords($arr, $sep=" "): string{
        $returnedString = "";
        for($i=0; $i< sizeof($arr); $i++){
                $returnedString .= $arr[$i];
                if($i!=sizeof($arr)-1){
                    $returnedString .= $sep;
                }
        }
        return $returnedString;
}
?>
