<?php
    function capsMe(string $str): string {
        return strtoupper($str);
    }
    function lowerMe(string $str): string {
        return strtolower($str);
    }
    function upperCaseFirst(string $str): string {
        return ucwords($str);
    }
    function lowerCaseFirst(string $str): string {
        $arr = explode(" ", $str);
        foreach ($arr as &$value) {
            $value = lcfirst($value);
        }
        return join(" ", $arr);
    }
    function removeBlankSpace(string $str): string {
        return trim($str, " ");
    }
?>
