<?php
    function isPangram(string $str): bool {
        $lstr = strtolower($str);
        foreach(range('a', 'z') as &$letter) {
                if(!str_contains($lstr, $letter)) {
                    return False;
                }
        }
        return True;
    }
?>
