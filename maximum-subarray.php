<?php
    function findMaximumSubarray ($arr): int | float {
        $bestSum = 0;
        $currentSum = 0;        
        foreach($arr as $x){
                $currentSum = max(0, $currentSum+$x);
                $bestSum = max($bestSum, $currentSum);
        }
        return $bestSum;
    }
?>
